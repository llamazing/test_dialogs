-- This script provides configuration information about text and languages.
--
-- Usage:
-- local language_manager = require("scripts/language_manager")

local language_manager = {}

local default_language = "en"

-- Returns the id of the default language.
function language_manager:get_default_language()
  return default_language
end

-- Returns the font and font size to be used for dialogs
-- depending on the specified language (the current one by default).
function language_manager:get_dialog_font(language)
  language = language or sol.language.get_language()

  if language == "chinese" then
    return "GnuUnifontFull", 16, 19
  elseif language == "russian" then
    return "basis33", 16
  else
    return "enter_command", 17
  end
end

-- Returns the width and height in pixels of the frame drawn for the dialog
-- box menu depending on the specified language (the current one by default).
function language_manager:get_dialog_box_size(language)
  language = language or sol.language.get_language()

  if language == "chinese" then
    return 320, 88
  --elseif language == "russian" then
  --  return 304, 80
  else
    return 304, 80
  end
end

-- Returns the font and font size to be used to display text in menus
-- depending on the specified language (the current one by default).
function language_manager:get_menu_font(language)
  language = language or sol.language.get_language()

  if language == "chinese" then
    return "GnuUnifontFull", 16, 19
  elseif language == "russian" then
    return "basis33", 16
  else
    return "oceansfont", 17
  end
end

-- Returns the font, font size and line offset to be used for text in the
-- world map depending on the specified language (the current one by default).
function language_manager:get_map_font(language)
  language = language or sol.language.get_language()

  if language == "chinese" then
    return "GnuUnifontFull", 16
  elseif language == "russian" then
    return "basis33", 14
  else
    return "CartographerTiny", 7, 6
  end
end

-- Returns the font, font size and banner height to be used for text in the
-- map banner depending on the specified language (the current one by default).
function language_manager:get_banner_font(language)
  language = language or sol.language.get_language()

  if language == "chinese" then
    return "GnuUnifontFull", 24, 28
  elseif language == "russian" then
    return "basis33", 24, 28
  else
    return "oceansfont_medium", nil, 28
  end
end

-- Returns the font and font size to be used to display text on HUD icons
-- depending on the specified language (the current one by default).
function language_manager:get_hud_icons_font(language)

  -- No font differences between languages (for now).
  return "enter_command", 16
end

return language_manager
