--[[ dialog_test.lua
	version 1.0.0
	31 Aug 2020
	GNU General Public License Version 3
	author: Llamazing

	   __   __   __   __  _____   _____________  _______
	  / /  / /  /  | /  |/  /  | /__   /_  _/  |/ / ___/
	 / /__/ /__/ & |/ , ,  / & | ,:',:'_/ // /|  / /, /
	/____/____/_/|_/_/|/|_/_/|_|_____/____/_/ |_/____/

	This script measures the width of the text for all dialogs (in all languages), looking
	to see if any exceed the maximum width. Intended for use as a test in the `quest-test`
	CI/CD pipeline.
	
	Rather than throwing an error immediately whenever a long dialog is encountered (which
	halts the test), a descriptive message gets output instead and an error will be thrown
	once all of the dialogs have been tested. This allows the test to document all dialogs
	that are too long as opposed to just the first one (whereby the test must be re-run to
	see if any more problems exist).
]]

local language_manager = require"scripts/language_manager"

local map = ...
local game = map:get_game()

local num_errors = 0 --keep track of total number of errors encountered for final report

--// Writes error message to stdout without triggering an error (so test not interrupted by error)
	--condition (any) - condition to test; if false or nil then an error has occurred
	--msg (string) - error message to output if condition fails
local function assert_to_log(condition, msg)
	if not condition then
		num_errors = num_errors + 1
		io.write("\027[1;31mERROR:\027[0m "..msg.."\n")
	end
	
	return not not condition
end

--// Checks all dialogs in the specified language to see if the width is too long
local function check_dialogs(language)
	language = language or sol.language.get_language() or 'en'
	assert(type(language)=="string", "Bad arguement #1 to 'check_dialogs' (string expected)")
	
	local path = string.format("languages/%s/text/dialogs.dat", language)
	local font, font_size = language_manager:get_dialog_font(language)
	local box_width, box_height = language_manager:get_dialog_box_size(language)
	local MAX_ALLOWED_WIDTH = box_width - 16
	
	local dialogs = {}
	local export = {}
	
	--load dialogs.dat for the specified language
	if sol.file.exists(path) then
		local env = {}
		
		function env.dialog(properties)
			local id = properties.id
			if not assert_to_log(id, "dialog entry is missing id") then return end
			if not assert_to_log(type(id)=="string", "dialog id must be a string: "..id) then return end
			if not assert_to_log(not dialogs[id], "duplicate dialog id ("..language.."): "..id) then return end
			
			table.insert(dialogs, properties)
			dialogs[id] = true
		end
		
		local chunk = sol.main.load_file(path)
		if not assert_to_log(chunk, "unable to load dialogs file (syntax error): "..path) then return end
		setfenv(chunk, env)
		chunk()
	end
	
	--check all dialogs
	for i,properties in ipairs(dialogs) do
		local id = properties.id
		local text = properties.text
		if assert_to_log(text, "Error: dialog "..id.." is missing text") then
			if assert_to_log(type(text)=="string", "Error: dialog "..id.." text must be a string") then
				text = text:gsub("\r\n", "\n"):gsub("\r", "\n")
				
				if not id:match"^quest%." then --ignore quest log dialogs
					local max_width = 0
					--local total_height = 0
					for line in text:gmatch"([^\n]*)\n" do
						local width, height = sol.text_surface.get_predicted_size(font, font_size, line)
						if width > max_width then max_width = width end
						--total_height = total_height + height
					end
					
					assert_to_log(max_width<=MAX_ALLOWED_WIDTH, string.format(
						"dialog width %d exceeds %d: (%s) %s",
						max_width,
						MAX_ALLOWED_WIDTH,
						language,
						id
					))
				end
			end
		end
	end
end

--// Does the dialog check for all languages present in the quest
local function check_dialogs_all_languages()
	for _,language in ipairs(sol.language.get_languages()) do
		check_dialogs(language)
	end
end

--// Start the test when map is started
function map:on_started()
	check_dialogs_all_languages()
	assert(num_errors==0, string.format("Dialog Test: %d errors encountered (see above)", num_errors))
	sol.main.exit()
end

--[[ Copyright 2020 Llamazing
  [] 
  [] This program is free software: you can redistribute it and/or modify it under the
  [] terms of the GNU General Public License as published by the Free Software Foundation,
  [] either version 3 of the License, or (at your option) any later version.
  [] 
  [] It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  [] without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  [] PURPOSE.  See the GNU General Public License for more details.
  [] 
  [] You should have received a copy of the GNU General Public License along with this
  [] program.  If not, see <http://www.gnu.org/licenses/>.
  ]]
